import 'package:basic_quiz_app/pages/quiz-page/quiz_controller.dart';
import 'package:basic_quiz_app/pages/quiz-page/quiz_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final imagesNamesList = [
      ["bash", "Bash"],
      ["devops", "Devops"],
      ["docker", "Docker"],
      ["html", "HTML"],
      ["linux", "Linux"],
      ["php", "PHP"],
      ["sql-server", "SQL"],
      ["wordpress", "WordPress"],
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Basic Quizz App"),
          titleTextStyle: const TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.italic),
          backgroundColor: Color(int.parse("0xFF6c757d")),
          centerTitle: true,
        ),
        backgroundColor: Color(int.parse("0xFFced4da")),
        body: GridView.count(
            crossAxisCount: 2,
            children: List<Widget>.generate(
                imagesNamesList.length,
                (index) => _singleQuizContainer(
                    imageName: imagesNamesList[index][0],
                    name: imagesNamesList[index][1]))));
  }

  _singleQuizContainer({required String imageName, required String name}) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: GestureDetector(
        onTap: () => _openQuizPage(name),
        child: Material(
            elevation: 5,
            borderRadius: BorderRadius.circular(10),
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Color(int.parse("0xFF6c757d")),
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    child: SizedBox(
                        width: 80,
                        height: 80,
                        child: Image.asset(
                          "assets/$imageName.png",
                          fit: BoxFit.cover,
                        )),
                  ),
                  const Spacer(),
                  Text(
                    name,
                    style: TextStyle(
                        fontSize: 28, color: Color(int.parse("0xFFced4da"))),
                  )
                ],
              ),
            )),
      ),
    );
  }

  _openQuizPage(String name) async {
    QuizController controller = Get.find();

    _loadingAnimation();

    await controller.getQuizList(name);

    Get.back();
    Get.to(QuizPage(name: name));
  }

  _loadingAnimation() {
    Get.defaultDialog(
      title: "Please Wait",
      backgroundColor: Color(int.parse("0xFFced4da")),
      content: SizedBox(
        height: 100,
        width: 100,
        child: CircularProgressIndicator(
          backgroundColor: Color(int.parse("0xFFced4da")),
          color: Color(int.parse("0xFF343a40")),
        ),
      ),
    );
  }
}
