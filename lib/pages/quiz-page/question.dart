import 'dart:developer';

class Question {
  late int id;
  late String question;
  String? description;
  late Map<String, dynamic> answers;
  // ignore: non_constant_identifier_names
  bool multiple_correct_answers = false;
  // ignore: non_constant_identifier_names
  late Map<String, dynamic> correct_answers;
  String? explaination;
  // ignore: non_constant_identifier_names
  late String? correct_answer;

  Question.fromJson(Map<String, dynamic> json) {
    // log(json.toString());
    id = json['id'];
    question = json['question'];
    description = json['description'];

    answers = {
      'answer_a': json['answers']['answer_a'],
      'answer_b': json['answers']['answer_b'],
      'answer_c': json['answers']['answer_c'],
      'answer_d': json['answers']['answer_d'],
      'answer_e': json['answers']['answer_e'],
      'answer_f': json['answers']['answer_f'],
    };

    multiple_correct_answers = bool.parse(json['multiple_correct_answers']);

    correct_answers = {
      'answer_1': bool.parse(json['correct_answers']['answer_a_correct']),
      'answer_2': bool.parse(json['correct_answers']['answer_b_correct']),
      'answer_3': bool.parse(json['correct_answers']['answer_c_correct']),
      'answer_4': bool.parse(json['correct_answers']['answer_d_correct']),
      'answer_5': bool.parse(json['correct_answers']['answer_e_correct']),
      'answer_6': bool.parse(json['correct_answers']['answer_f_correct']),
    };

    explaination = json['explanation'];

    correct_answer = json['correct_answer'];
  }
}
