import 'package:basic_quiz_app/pages/quiz-page/quiz_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuizPage extends StatelessWidget {
  final String name;
  QuizPage({required this.name, super.key});

  final QuizController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        backgroundColor: Color(int.parse("0xFFced4da")),
        body: SafeArea(
          child: ListView(
            children: [
              Container(
                height: 40,
                margin: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      width: 3,
                      color: Color(int.parse("0xFF343a40")),
                    )),
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                          width: 40,
                          child: Obx(
                            () => LinearProgressIndicator(
                              value: controller.currentIndex.value / 10,
                              backgroundColor: Colors.white,
                              color: Colors.yellow,
                              minHeight: 15,
                              borderRadius: BorderRadius.circular(10),
                            ),
                          )),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Obx(
                      () => Text(
                        "${controller.currentIndex.value} / 10",
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(int.parse("0xFF343a40")),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15),
                child: Material(
                  elevation: 8,
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    height: 300,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Color(int.parse("0xFF6c757d")),
                        borderRadius: BorderRadius.circular(10)),
                    // margin: const EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Expanded(
                            flex: 2,
                            child: Center(
                              child: Obx(() => Text(
                                    controller.currentQuestion.value.question,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Color(int.parse("0xFFced4da")),
                                    ),
                                  )),
                            )),
                        Divider(
                          thickness: 3,
                          color: Color(int.parse("0xFFced4da")),
                        ),
                        Expanded(
                            flex: 1,
                            child: Center(
                              child: Obx(
                                () => controller.currentQuestion.value
                                            .description ==
                                        null
                                    ? Text(
                                        "No Description",
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Color(int.parse("0xFFced4da")),
                                        ),
                                      )
                                    : Text(
                                        controller
                                            .currentQuestion.value.description!,
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Color(int.parse("0xFFced4da")),
                                        ),
                                      ),
                              ),
                            )),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              _optionContainer(optionString: "answer_a", index: 1),
              _optionContainer(optionString: "answer_b", index: 2),
              _optionContainer(optionString: "answer_c", index: 3),
              _optionContainer(optionString: "answer_d", index: 4),
            ],
          ),
        ),
      ),
    );
  }

  _optionContainer({required String optionString, required int index}) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 15),
      child: GestureDetector(
        onTap: () => controller.checkAnswer(index),
        child: Material(
            elevation: 8,
            borderRadius: BorderRadius.circular(10),
            child: Obx(
              () => AnimatedContainer(
                  duration: const Duration(milliseconds: 500),
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: controller.boxColors[index - 1],
                      borderRadius: BorderRadius.circular(10)),
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Color(int.parse("0xFFe5e5e5")),
                            shape: BoxShape.circle),
                        padding: const EdgeInsets.all(10),
                        child: Text(
                          index.toString(),
                          style: TextStyle(
                              color: Color(
                                int.parse("0xFF343a40"),
                              ),
                              fontSize: 18),
                        ),
                      ),
                      Expanded(
                        child: Obx(
                          () => Text(
                            controller.currentQuestion.value
                                    .answers[optionString] ??
                                "No Option",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              color: Color(int.parse("0xFFced4da")),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            )),
      ),
    );
  }
}
