import 'dart:convert';
import 'dart:developer';

import 'package:basic_quiz_app/pages/home-page/home_page.dart';
import 'package:basic_quiz_app/pages/quiz-page/question.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class QuizController extends GetxController {
  List<Question> _questionList = [];
  RxInt currentIndex = 0.obs;

  int score = 0;

  late Rx<Question> currentQuestion = _questionList[currentIndex.value].obs;

  Map<String, String> urlMap = {
    'Bash':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=bash&limit=10",
    'Devops':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=devops&limit=10",
    'Docker':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=docker&limit=10",
    'HTML':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=code&limit=10",
    'Linux':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=linux&difficulty=Easy&limit=10",
    'PHP':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=cms&limit=10",
    'SQL':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=sql&limit=10",
    'WordPress':
        "https://quizapi.io/api/v1/questions?apiKey=jja1KjROpArHZbdC9CBvMYvkv7znf13RkHPQ3uMt&category=cms&limit=10",
  };

  Future<void> getQuizList(String name) async {
    String url = urlMap[name]!;
    log(url);
    try {
      final response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        final List<dynamic> responseList = jsonDecode(response.body);

        // log(responseList.length.toString());
        // log(responseList[0].runtimeType.toString());

        _questionList = List<Question>.generate(responseList.length,
            (index) => Question.fromJson(responseList[index]));

        currentQuestion.value = _questionList[0];
        currentIndex.value = 0;
      } else {
        log("HTTP request failed with status: ${response.statusCode}");
      }
    } catch (e) {
      log("Exception = $e");
    }
  }

  void setDefalut() {
    boxColors[0] = Color(int.parse("0xFF495057"));
    boxColors[1] = Color(int.parse("0xFF495057"));
    boxColors[2] = Color(int.parse("0xFF495057"));
    boxColors[3] = Color(int.parse("0xFF495057"));

    currentIndex.value = 0;
  }

  void nextQuestion() {
    if (currentIndex.value < 9) {
      currentIndex.value++;
      currentQuestion.value = _questionList[currentIndex.value];

      boxColors[0] = Color(int.parse("0xFF495057"));
      boxColors[1] = Color(int.parse("0xFF495057"));
      boxColors[2] = Color(int.parse("0xFF495057"));
      boxColors[3] = Color(int.parse("0xFF495057"));
    } else {
      currentIndex.value++;

      showScoreScreen();
      setDefalut();
    }
  }

  void previousQuestion() {
    if (currentIndex.value > 0) {
      currentIndex.value++;
      currentQuestion.value = _questionList[currentIndex.value];
    } else {
      Get.showSnackbar(const GetSnackBar(
        message: "Invalid Index",
      ));
    }
  }

  RxList<Color> boxColors = <Color>[
    Color(int.parse("0xFF495057")),
    Color(int.parse("0xFF495057")),
    Color(int.parse("0xFF495057")),
    Color(int.parse("0xFF495057")),
  ].obs;

  void checkAnswer(int index) async {
    log("Here");
    Map<String, dynamic> correct_answers =
        currentQuestion.value.correct_answers;

    log(correct_answers.toString());

    if (correct_answers['answer_$index'] == true) {
      boxColors[index - 1] = Colors.green;
      score++;
    } else {
      boxColors[index - 1] = Colors.red;
    }

    for (int i = 0; i < 4; i++) {
      if (correct_answers['answer_${i + 1}'] == true) {
        boxColors[i] = Colors.green;
      }
    }
    await Future.delayed(const Duration(seconds: 2));

    nextQuestion();
  }

  void showScoreScreen() async {
    await Future.delayed(const Duration(seconds: 1));
    Get.defaultDialog(
        title: "Your Score",
        content: Text(
          "$score / 10",
          style: const TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w500,
          ),
        ));
    await Future.delayed(const Duration(seconds: 5));

    Get.offAll(const HomePage());
  }
}
